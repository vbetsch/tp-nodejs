node_modules:
	npm install

dev: node_modules
	npm run dev

test: node_modules
	npm run test

newman: node_modules
	newman run ./tests/TP.postman_collection.json

.PHONY: dev test newman
