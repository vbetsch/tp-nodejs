# TP NodeJS

Install node modules
```bash
npm install
```

Run tests
```bash
npm run test
```

Run tests in dev mode
```bash
npm run dev
```

Run Postman tests
```bash
newman run ./tests/TP.postman_collection.json
```
